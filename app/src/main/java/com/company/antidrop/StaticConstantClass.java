package com.company.antidrop;

import android.content.Context;

// Constant file
public class StaticConstantClass {

    public static String GetlinkWeb(Context mContext){
        return mContext.getString(R.string.link_webSite);
    }

    public static String GetHtmlCode(Context mContext){
        return "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "<meta charset=\"utf-8\">\n" +
                "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "\n" +
                "<title>Pas de connexion</title>\n" +
                "\n" +
                "\n" +
                "</head>\n" +
                "<body>\n" +
                "        <style>\n" +
                "                html {font-size: 18px;}\n" +
                "                body { \n" +
                "                  font-family: 'Source Sans Pro', sans-serif;\n" +
                "                  font-size: 1em;position:fixed;top:0\n" +
                "                }\n" +
                "                \n" +
                "                #wrapper {\n" +
                "                  width: 410px;\n" +
                "                  height: 350px;\n" +
                "                  margin: 0 auto;\n" +
                "                  overflow: hidden;\n" +
                "                  \n" +
                "                  position: absolute;\n" +
                "                  top: 300px;\n" +
                "                  left: 50%;\n" +
                "                  margin-top: -175px;\n" +
                "                  margin-left: -205px;\n" +
                "                }\n" +
                "                \n" +
                "                \n" +
                "                .content {\n" +
                "                  width: 100%;\n" +
                "                  margin: 0 auto;\n" +
                "                  text-align: center;\n" +
                "                }\n" +
                "                \n" +
                "                h1 {\n" +
                "                  font-weight: 300;\n" +
                "                  font-size: 1.5em;\n" +
                "                  color: #000;\n" +
                "                }\n" +
                "                h4 {\n" +
                "                  font-weight: 100;\n" +
                "                  font-size: 1em;\n" +
                "                  color: #000;\n" +
                "                }\n" +
                "                \n" +
                "                p {\n" +
                "                  font-family: 'Source Sans Pro', sans-serif;\n" +
                "                  line-height: 1em;\n" +
                "                  font-weight: 300;\n" +
                "                  color: #333;\n" +
                "                }\n" +
                "                \n" +
                "                .center {\n" +
                "                        display: flex;\n" +
                "                        justify-content: center;\n" +
                "                        align-items: center;\n" +
                "                }\n" +
                "                /* \n" +
                "                Server by @chrisburton\n" +
                "                */\n" +
                "                \n" +
                "                .grid {\n" +
                "                  max-width: 175px;\n" +
                "                  height: 200px;\n" +
                "                  background: #222;\n" +
                "                  margin: 0 auto;\n" +
                "                  padding: 1em 0;\n" +
                "                  border-radius: 3px;\n" +
                "                }\n" +
                "                \n" +
                "                \n" +
                "                .grid .server {\n" +
                "                  display: block;\n" +
                "                  max-width: 68%;\n" +
                "                  height: 20px;\n" +
                "                  background: rgba(255,255,255,.15);\n" +
                "                  box-shadow: 0 0 0 1px black inset;\n" +
                "                  margin: 10px 0 20px 30px;\n" +
                "                }\n" +
                "                \n" +
                "                .grid .server:before {\n" +
                "                  content: \"\";\n" +
                "                  position: relative;\n" +
                "                  top: 7px;\n" +
                "                  left: -18px;\n" +
                "                  display: block;\n" +
                "                  width: 6px;\n" +
                "                  height: 6px;\n" +
                "                  background: green;\n" +
                "                  border: 1px solid black;\n" +
                "                  border-radius: 6px;\n" +
                "                  margin-top: 7px;\n" +
                "                }\n" +
                "                \n" +
                "                /* Animation */\n" +
                "                \n" +
                "                @-webkit-keyframes pulse {\n" +
                "                  0% {background: rgba(255,255,255,.15);}\n" +
                "                  100% {background: #ae1508;}\n" +
                "                }\n" +
                "                \n" +
                "                .grid .server:nth-child(3):before {\n" +
                "                  background: rgba(255,255,255,.15);\n" +
                "                  -webkit-animation: pulse .5s infinite alternate;\n" +
                "                }\n" +
                "                \n" +
                "                @-webkit-keyframes pulse_three {\n" +
                "                  0% {background: rgba(255,255,255,.15);}\n" +
                "                  100% {background: #d2710a;}\n" +
                "                }\n" +
                "                \n" +
                "                .grid .server:nth-child(5):before {\n" +
                "                  background: rgba(255,255,255,.15);\n" +
                "                  -webkit-animation: pulse_three .7s infinite alternate;\n" +
                "                }\n" +
                "                \n" +
                "                @-webkit-keyframes pulse_two {\n" +
                "                  0% {background: rgba(255,255,255,.15);}\n" +
                "                  100% {background: #9da506;}\n" +
                "                }\n" +
                "                .grid .server:nth-child(1):before {\n" +
                "                  background: rgba(255,255,255,.15);\n" +
                "                  -webkit-animation: pulse_two .1s infinite alternate;\n" +
                "                }\n" +
                "                .grid .server:nth-child(2):before {\n" +
                "                  background: rgba(255,255,255,.15);\n" +
                "                  -webkit-animation: pulse_two .175s infinite alternate;\n" +
                "                }\n" +
                "                .grid .server:nth-child(4):before {\n" +
                "                  background: rgba(255,255,255,.15);\n" +
                "                  -webkit-animation: pulse_two .1s infinite alternate;\n" +
                "                }\n" +
                "                \n" +
                "                @media only screen \n" +
                "                  and (min-device-width: 320px) \n" +
                "                  and (max-device-width: 480px)\n" +
                "                  and (-webkit-min-device-pixel-ratio: 2) {\n" +
                "                    html {font-size: 12px;}\n" +
                "                }\n" +
                "                @media only screen \n" +
                "                  and (min-device-width: 320px) \n" +
                "                  and (max-device-width: 568px)\n" +
                "                  and (-webkit-min-device-pixel-ratio: 2) {\n" +
                "                    html {font-size: 14px;}\n" +
                "                }\n" +
                "\n" +
                "                button{\n" +
                "                        padding: 1rem;\n" +
                "                        border: 0.5rem solid rebeccapurple; \n" +
                "                        border-radius: 0.3rem;\n" +
                "                        font-size: 1.5rem;\n" +
                "                        background: rebeccapurple;\n" +
                "                        color: white;\n" +
                "                }\n" +
                "\n" +
                "                \n" +
                "                \n" +
                "                \n" +
                "                </style>\n" +
                "                <h1 style=\"text-align: center;\">Erreur de connexion</h1>\n" +
                "                <p style=\"text-align: center;\">Vérifiez que vous avez bien accès à internet.</p>\n" +
                "                <p style=\"text-align: center;\">Si c'est le cas, cela signifie que nos serveurs sont surchargés.</p>\n" +
                "\n" +
                "\n" +
                "        <div id=\"wrapper\">\n" +
                "\n" +
                "        \n" +
                "                <div class=\"grid\">\n" +
                "                  <span class=\"server\"></span>\n" +
                "                  <span class=\"server\"></span>\n" +
                "                  <span class=\"server\"></span>\n" +
                "                  <span class=\"server\"></span>\n" +
                "                  <span class=\"server\"></span>\n" +
                "                </div>\n" +
                "                \n" +
                "                \n" +
                "                </div>  \n" +
                "                <h4  style=\"text-align: center; margin-top: 280px; margin-left: 50px;margin-right: 50px;\">Ne désinstallez pas l'application, elle sera fonctionnelle dès que nos serveurs seront de nouveau en état de marche ;)</h4>\n" +
                "                <div class=\"center\">\n" +
                "                        <button onclick=\"window.JSInterface.onJsAlert()\">Recharger la page</button>\n" +
                "                      </div>\n" +
                "\n" +
                "</body>\n" +
                "</html>\n" +
                "\n";
    }
}

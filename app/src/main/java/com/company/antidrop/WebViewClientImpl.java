package com.company.antidrop;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebViewClientImpl extends WebViewClient {

    private Activity activity = null;
    String ShowOrHideWebViewInitialUse = "show";

    public WebViewClientImpl(Activity activity) {
        this.activity = activity;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView webView, String url) {
        if(url.contains(StaticConstantClass.GetlinkWeb(activity))){
            return false;
        }

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        activity.startActivity(intent);
        return true;
    }

    @Override
    public void onPageStarted(WebView webview, String url, Bitmap favicon) {

        // only make it invisible the FIRST time the app is run
        if (ShowOrHideWebViewInitialUse.equals("show")) {
            webview.setVisibility(webview.INVISIBLE);
        }
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        ShowOrHideWebViewInitialUse = "hide";
        view.setVisibility(WebView.VISIBLE);
        super.onPageFinished(view, url);
    }
}
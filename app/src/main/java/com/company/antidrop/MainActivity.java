package com.company.antidrop;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private WebView mWebView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);

        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        if(Objects.requireNonNull(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)).getState() == NetworkInfo.State.CONNECTED ||
                Objects.requireNonNull(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network, so we lunch the webview

            setContentView(R.layout.activity_main);

            WebViewClientImpl webViewClient = new WebViewClientImpl(this);

            mWebView = findViewById(R.id.webview);
            WebSettings webSettings = mWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);

            mWebView.setWebViewClient(webViewClient);

            mWebView.loadUrl(StaticConstantClass.GetlinkWeb(this));
        }
        else
        {
            // we are not connected to a network, so we lunch the no connection activity
            Intent intent = new Intent(this,NoConnection.class);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        if (mWebView.copyBackForwardList().getCurrentIndex() > 0) {
            mWebView.goBack();
        }
        else {
            super.onBackPressed(); // finishes activity
        }
    }
}
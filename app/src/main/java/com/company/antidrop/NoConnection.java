package com.company.antidrop;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class NoConnection extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_connection);

        WebView mWebView = findViewById(R.id.webViewConnexion);

        mWebView.setWebViewClient(new WebViewClient());

        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        //Javascript interface for call java function with a javascript function
        JavaScriptInterface jsInterface = new JavaScriptInterface(this);
        mWebView.addJavascriptInterface(jsInterface, "JSInterface");

        mWebView.getSettings().setAllowFileAccess(true);

        String codeHtml = StaticConstantClass.GetHtmlCode(this);

        mWebView.loadDataWithBaseURL(null,codeHtml,"text/html", "utf-8", null);
    }
}